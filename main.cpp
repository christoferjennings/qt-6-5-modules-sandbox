#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QTranslator>
#include <QtQml>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.addImportPath(":/christoferjennings.com/imports");
    const QUrl url(u"qrc:/christoferjennings.com/imports/Sandbox/main.qml"_qs);
    engine.load(url);

    return app.exec();
}
