import QtQuick
import Base

Window {
    id: window
    width: 500
    height: 300
    visible: true
    title: qsTr("Qt 6.5 QML modules sandbox")
    color: MyConstants.aColor

    Rectangle {
        anchors.centerIn: parent
        width: 400
        height: 250
        color: "yellow"

        StackView {
            id: stackView
            initialItem: "page1.qml"
            anchors.fill: parent
        }
    }
}