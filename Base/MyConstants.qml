pragma Singleton

import QtQuick

QtObject {
    readonly property int aNumber: 13
    readonly property color aColor: "teal"
    readonly property string someWords: "Words from MyConstants."
}