import QtQuick
import QtQuick.Layouts
import Base
import SomeFeature

Page {
    id: thePage

    Rectangle {
        color: "red"
        anchors.fill: parent

        MyCPPThing {
            id: cppThing
        }

        ColumnLayout {
            id: myColumn
            spacing: MyConstants.aNumber
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right

            Text {
                id: simpleString
                width: parent.width
                text: "We're going on an adventure."
            }

            Text {
                id: fromMyConstantsQML
                width: parent.width
                text: MyConstants.someWords
            }

            Text {
                id: fromMyCPPThing
                width: parent.width
                text: cppThing.something()
            }

            Button {
                id: theButton
                text: "go"
                onClicked: thePage.StackView.view.push("qrc:/christoferjennings.com/imports/SomeFeature/SomeFeaturePage1.qml")
            }
        }
    }
}