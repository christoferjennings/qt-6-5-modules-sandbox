#ifndef MY_CPP_THING_H
#define MY_CPP_THING_H

#include <QObject>
#include <QtQml/qqmlregistration.h>

class MyCPPThing : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString m_something READ something WRITE setSomething NOTIFY somethingChanged)
    QML_ELEMENT

public:
    explicit MyCPPThing(QObject *parent = nullptr);

public slots:
    QString something() const;
    void setSomething(const QString val);

signals:
    void somethingChanged();

private:
    QString m_something {"Words from MyCPPThing."};
};

#endif // MY_CPP_THING_H
