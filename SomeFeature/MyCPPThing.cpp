#include "MyCPPThing.h"
#include <QtQml>

MyCPPThing::MyCPPThing(QObject *parent)
    : QObject(parent)
{
}
QString MyCPPThing::something() const
{
    QQmlEngine::contextForObject(this)->isValid();
    return m_something;
}
void MyCPPThing::setSomething(const QString val) {
    m_something = val;
}