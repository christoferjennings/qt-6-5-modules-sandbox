import QtQuick
import QtQuick.Layouts
import Base

Page {
    id: thePage

    Rectangle {
        color: "green"
        anchors.fill: parent

        MyCPPThing {
            id: cppThing
        }

        ColumnLayout {
            id: myColumn
            spacing: MyConstants.aNumber

            Text {
                id: simpleString
                width: parent.width
                text: "This is SomeFeaturePage1.qml."
            }

            Text {
                id: fromMyCPPThing
                width: parent.width
                text: cppThing.something()
            }

            Text {
                id: fromMyConstantsQML
                width: parent.width
                text: MyConstants.someWords
            }

            Button {
                id: theButton
                text: "go back"
                onClicked: thePage.StackView.view.pop()
            }
        }
    }
}